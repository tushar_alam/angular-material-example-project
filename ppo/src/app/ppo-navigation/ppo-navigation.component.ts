import { UserService } from './../user/shared/user.service';
import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ppo-navigation',
  templateUrl: './ppo-navigation.component.html',
  styleUrls: ['./ppo-navigation.component.css']
})
export class PpoNavigationComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  userName = "User";
  constructor(private breakpointObserver: BreakpointObserver,
    private router: Router, public userService: UserService) { }

  ngOnInit(): void {
    this.userName= JSON.parse(localStorage.getItem('name')) ? JSON.parse(localStorage.getItem('name')) : 'User'
  }

  logOut() {
    localStorage.clear();
    this.router.navigate(['/user/signin']);
  }
}
