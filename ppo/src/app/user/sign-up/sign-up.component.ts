import { Component, OnInit } from '@angular/core';
import { User } from "../shared/user.model";
import { FormGroup, FormControl, FormBuilder, Validator, Validators } from "@angular/forms";
import { Router } from "@angular/router"

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  hide = true;
  signUpForm: FormGroup;
  user: User = new User();
  constructor(private route:Router) { }


  ngOnInit() {
    this.signUpForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(8)]),
      gender: new FormControl(''),
    });
  }
  
  onSubmit() {
    console.warn(this.signUpForm.value);
    this.route.navigate(['/ppo']);
  }

  getErrorMessage() {
    return this.signUpForm.controls.email.hasError('required') ? 'You must enter a value' :
        this.signUpForm.controls.email.hasError('email') ? 'Not a valid email' :
            '';
  }
}
