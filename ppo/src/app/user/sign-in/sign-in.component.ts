import { UserService } from './../shared/user.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from "@angular/router"
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  hide = true;
  signInForm: FormGroup;
  isLoginError : boolean = false;
  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.signInForm = new FormGroup({
      userName: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(20)])
    });
  }

  OnSubmit() {
    var data=this.signInForm.value;
    this.userService.userAuthentication(data.userName, data.password).subscribe((data: any) => {
      localStorage.setItem('userToken', data.access_token);
      localStorage.setItem('userRoles', data.role);
      localStorage.setItem('name', data.name);
      this.router.navigate(['/ppo'])
    },
      (err: HttpErrorResponse) => {
        this.isLoginError = true;
      });
  }

  getErrorMessage() {
    return this.signInForm.controls.email.hasError('required') ? 'You must enter a value' :
      this.signInForm.controls.email.hasError('email') ? 'Not a valid email' :
        '';
  }
}
