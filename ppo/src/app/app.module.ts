import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MyMaterialModule } from './my-material/my-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth/auth.interceptor';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { PpoNavigationComponent } from './ppo-navigation/ppo-navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { HomeComponent } from './ppo-navigation/home/home.component';
import { AuthGuard } from './auth/auth.guard';
import { ReportComponent } from './ppo-navigation/report/report.component';

import { FileComponent } from './file/file.component';
import { VideoComponent } from './file/video/video.component';
import { PdfComponent } from './file/pdf/pdf.component'
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';



@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    SignInComponent,
    SignUpComponent,
    HomeComponent,
    PpoNavigationComponent,
    ReportComponent,
    VideoComponent,
    PdfComponent,
    FileComponent,
    ForbiddenComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MyMaterialModule,
    NgbModule.forRoot(),
    PdfViewerModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    LayoutModule,
  ],
  providers: [AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
