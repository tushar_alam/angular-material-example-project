import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FileComponent } from './file/file.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { PpoNavigationComponent } from './ppo-navigation/ppo-navigation.component';
import { HomeComponent } from './ppo-navigation/home/home.component';
import { AuthGuard } from './auth/auth.guard';
import { ReportComponent } from './ppo-navigation/report/report.component';
import { VideoComponent } from './file/video/video.component';
import { PdfComponent } from './file/pdf/pdf.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';

const routes: Routes = [

  {
    path: 'ppo', component: PpoNavigationComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'report', component: ReportComponent, canActivate: [AuthGuard], data: { roles: ['SuperAdmin'] } },
      {
        path: 'file', component: FileComponent,
        children: [
          { path: 'video', component: VideoComponent, canActivate: [AuthGuard], data: { roles: ['SuperAdmin','Admin'] }, },
          { path: 'pdf', component: PdfComponent, canActivate: [AuthGuard], data: { roles: ['SuperAdmin','Admin'] }, }
        ]
      },
    ]
  },
  {
    path: 'user', component: UserComponent,
    children: [
      { path: 'signup', component: SignUpComponent },
      { path: 'signin', component: SignInComponent }
    ]
  },
  { path: 'forbidden', component: ForbiddenComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: 'user/signin', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
