export class FileVm {
    id:number;
    title:string;
    description:string;
    type:number;
    url:string;
    role_id:number;
}
