import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppShared } from 'src/app/appShared';
import { FileVm } from './file.model';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  appShared: AppShared = new AppShared;
  constructor(private http: HttpClient) { }

  getFiles() {
    return this.http.get(this.appShared.rootUrl + "/api/Files/GetFiles");
  }
}
