import { FileService } from './../shared/file.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FileVm } from '../shared/file.model';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {
  @ViewChild('videoPlayer') videoplayer: ElementRef;
  files: FileVm[];

  constructor(private fileService: FileService) {
  }

  ngOnInit() {
    this.fileService.getFiles().subscribe((data: any) => {
      this.files = data.Files;
    },
      (error) => {
        console.log("something went wrong...");
      })
  }

  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }

}
