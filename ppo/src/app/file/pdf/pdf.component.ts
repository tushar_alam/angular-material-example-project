import { Component, OnInit } from '@angular/core';
import { FileService } from '../shared/file.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css']
})
export class PdfComponent implements OnInit {
  page: number = 1;
  files: any[];

  constructor(private fileService: FileService,public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.fileService.getFiles().subscribe((data: any) => {
        this.files = data.Files
    },
      (error) => {
        console.log("something went wrong...");
      })
  }
}
